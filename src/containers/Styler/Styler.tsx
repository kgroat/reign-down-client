
import * as React from 'react'
import { observer } from 'mobx-react-lite'
import { ColorProp, ColorName, allColors } from 'state/colorStore'
import { useStores } from 'state'

import ColorPicker from 'components/ColorPicker'
import Button from 'components/Button'

import { styler, open, chooser, toggler, closer } from './Styler.pcss'

interface Props extends ColorProp {
}

interface State {
  isOpen: boolean
  renderDom: boolean
  hideTimeout: any
}

const HIDE_DOM_TIMEOUT_MS = 210

export function Styler () {
  const { color } = useStores()
  const [isOpen, setOpen] = React.useState(false)
  const [renderDom, setRenderDom] = React.useState(false)
  const [hideTimeout, setHideTimeout] = React.useState<any>(null)

  function renderColorPicker (colorName: ColorName) {
    return (
      <ColorPicker
        key={colorName}
        id={`${colorName}ColorPicker`}
        label={colorName[0].toUpperCase() + colorName.substring(1)}
        onChange={val => color[colorName] = val}
        value={color[colorName]}
      />
    )
  }

  function renderChooser () {
    if (!renderDom) {
      return
    }

    return (
      <div className={chooser}>
        {allColors.map(renderColorPicker)}
        <div>
          <Button id='resetColors' color='blood' onClick={color.resetColors.bind(color)}>Reset colors</Button>
        </div>
      </div>
    )
  }

  function toggleOpen () {
    const shouldBeOpen = !isOpen
    if (shouldBeOpen) {
      if (hideTimeout) {
        clearTimeout(hideTimeout)
      }
      setRenderDom(true)
      setHideTimeout(null)
      requestAnimationFrame(() => {
        setOpen(shouldBeOpen)
      })
    } else {
      const hideTimeout = setTimeout(() => setRenderDom(false), HIDE_DOM_TIMEOUT_MS)
      setOpen(shouldBeOpen)
      setHideTimeout(hideTimeout)
    }
  }

  const classes = [styler]
  if (isOpen) classes.push(open)

  return (
    <div className={classes.join(' ')}>
      <div className={closer} onClick={() => setOpen(!isOpen)} />
      {renderChooser()}
      <Button id='stylerToggle' className={toggler} color='primary' onClick={toggleOpen}>
        C
      </Button>
    </div>
  )
}

export default observer(Styler)
