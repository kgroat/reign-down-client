import * as webpack from 'webpack'
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin'

import { buildConstants } from '../webpack/constants'
import { codeRule } from '../webpack/code'
import { styleRule } from '../webpack/styles'

type StorybookMode = 'DEVELOPMENT' | 'PRODUCTION'
interface StorybookControlMode {
  config: webpack.Configuration
  mode: StorybookMode
}

module.exports = ({ config }: StorybookControlMode) => {
  const constants = buildConstants(true)

  config.module!.rules.push(
    codeRule(true),
    {
      ...styleRule(true),
      test: /\.pcss$/,
    },
  )

  config.resolve!.extensions!.push('.ts', '.tsx')
  config.resolve!.plugins = [new TsconfigPathsPlugin({ configFile: constants.tsconfig })]
  return config
}
