
import * as React from 'react'
import { observer, inject } from 'mobx-react'
import { AuthProp } from 'state/authStore'

import { $lowerName } from './$UpperName.pcss'

interface Props extends AuthProp {
}

@inject('auth')
@observer
export default class $UpperName extends React.PureComponent<Props> {

  public render () {
    const { auth } = this.props

    return (
      <div className={$lowerName}>
        {
          auth!.jwt
          ? <div>You're logged in</div>
          : <div>You're not logged in</div>
        }
      </div>
    )
  }
}
