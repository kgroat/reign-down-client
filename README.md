# Reign Down Client

[Coverage Report](https://kgroat.gitlab.io/reign-down-client/coverage)

[Storybook](https://should-i-use.gitlab.io/reign-down-client/storybook)

## Color Palette
https://coolors.co/661422-c2fcbd-ddebff-fcea9c-d6be9e

## Quickstart

### Dependency installation
```bash
$ npm install
```

### Starting the app in HMR mode
```bash
$ npm start
```

### Starting the app in "mock gql" mode
In this mode, the application will not make requests to the backend. It will instead use `src/mockLink.ts` to resolve all requests made through the graphql client.
```bash
$ npm start dev
```

### Checking the validity of the app
NOTE: There isn't a solid setup for tests yet
```bash
# unit tests
$ npm test

# lint the app code & styles
$ npm start lint
```

To run a single test file, you can use the following shorthand syntax:
```bash
$ npm test MyComponent
```

### Other information about the scripts
Under the hood, `npm start` is simpy running the `nps` command.  If you wish, you can install this script globally using `npm i -g nps` and run the scripts by typing, for instance: `nps build`.  Globally installing `nps` also allows you to get auto-completion in a `bash` environment (or `zsh` with [`bashcompinit` enabled](https://stackoverflow.com/a/27853970/2939688)) by adding the following to your `.bash_profile` or `.bashrc` (or `.zshrc`):
```bash
source <(nps completion)
```

#### Running more than one script at a time
You can run multiple `package-scripts` script one after the other by putting them one after the other.
For instance, to run both linters and tests, you can use the following syntax:
```bash
$ npm start lint test
```

Each "word" after `npm start` is interpreted as a different command to be run

#### Running a script with parameters
You can run a command with parameters by wrapping the name of the command and its parameters in a string.
For instance, to run a single test file, you can use the following syntax:
```bash
$ npm start 'test MyComponent'
```

You can combine this syntax with running multiple scripts at once.
For instance, if you want to lint and test `Icon` with a single command:
```bash
$ npm start 'lint.ts src/**/Icon.tsx' 'test Icon'
```

#### To view all available scripts and execute one using an interactive shell, run:
```bash
$ npm run interactive
```

There's cool stuff in there like generating components!

## Using GitLab runner
You can run CI commands locally using `gitlab-runner`.  You'll first need to [install gitlab-runner](https://docs.gitlab.com/runner/install/index.html).

From then on, you can run any of the steps (i.e. `run-lint`, `run-tests`, `build-app`) in the `.gitlab-ci.yml` file by running:
```bash
gitlab-runner exec docker --pre-build-script 'npm i' [step]
```

You can update the `--pre-build-script` to `'npm i && npm start build'` if you need a build of the app before the step can be run

## Storybook
Used for building out components, introducing new contributors to the components, and
remembering how the components work. `npm run story` to start storybook at
localhost:9000.


### Storybook Plugin Docs
- [knobs](https://github.com/storybooks/storybook/tree/master/addons/knobs)
- [state](https://github.com/Sambego/storybook-state)

## Githooks
This repository makes use of the following hook:
* `pre-push`

It is automatically installed in your `.git` directory whenever you `npm install`.  A thin script located in the `.githooks` directory is what gets installed as the hook.  It is thin so that the meat of the script can live in `scripts/hooks`.  This way, any updates to the main script in `scripts/hooks` do not require it to be re-installed.
