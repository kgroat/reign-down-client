
import * as React from 'react'
import { shallow } from 'enzyme'

import { HeaderBase } from './Header'

describe(HeaderBase.name, () => {
  it('should mount without throwing an error', () => {
    expect(shallow(<HeaderBase auth={{} as any} />)).toMatchSnapshot()
  })
})
