
describe('Color picker', () => {
  it('sets the proper css variable on the root element', () => {
    const typedColor = '#abcdef'

    cy.visit(Cypress.config('baseUrl')!)
    cy.get('#stylerToggle').click()
    cy.get('#primaryColor-text').clear().type(typedColor)
    cy.shouldHaveCssVar('--primary', typedColor)
    cy.get('#resetColors').shouldHaveCssVar('--btn-background', typedColor)
  })
})
