
interface InstallEvent extends Event {
  prompt (): void
  userChoice: Promise<{ outcome: string }>
}

let deferredPrompt: null | InstallEvent = null

export let canInstall: boolean = __DEV__

export async function install () {
  __DEV__ && console.warn('cannot install the app while in local development mode')
  if (!deferredPrompt) {
    return
  }

  deferredPrompt.prompt()

  const choice = await deferredPrompt.userChoice

  if (choice.outcome === 'accepted') {
    __DEV__ && console.debug('User accepted installation')
  } else {
    __DEV__ && console.debug('User dismissed installation')
  }

  deferredPrompt = null
  canInstall = false
}

if (!__DEV__ && 'serviceWorker' in navigator) {
  // Use the window load event to keep the page load performant
  window.addEventListener('load', async () => {
    try {
      await navigator.serviceWorker.register('./service-worker.js')
    } catch (e) {
      console.debug('Something went wrong while registering the service worker', e)
    }
  })

  window.addEventListener('beforeinstallprompt', (evt: InstallEvent) => {
    evt.preventDefault()
    deferredPrompt = evt
    canInstall = true
  })
}
