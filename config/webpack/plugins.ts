
import * as webpack from 'webpack'
import * as path from 'path'

import * as HtmlWebpackPlugin from 'html-webpack-plugin'
import * as MiniCssExtractPlugin from 'mini-css-extract-plugin'
import * as CaseSensitivePathsPlugin from 'case-sensitive-paths-webpack-plugin'
import * as CopyWebpackPlugin from 'copy-webpack-plugin'
import { InjectManifest } from 'workbox-webpack-plugin'
import { WatchMissingNodeModulesPlugin } from './WatchMissingNodeModulesPlugin'

import { Constants } from './constants'

// no type definitions exist for workbox-webpack-plugin

export function buildPlugins (dev: boolean, mockGql: boolean, constants: Constants) {
  let plugins = [
    new HtmlWebpackPlugin({
      inject: 'body',
      template: constants.htmlPath,
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
      'process.env.GRAPHQL_URL': JSON.stringify(process.env.GRAPHQL_URL || 'http://localhost:3001/graphql'),
      'process.env.GRAPHQL_SUBSCRIPTION_URL': JSON.stringify(process.env.GRAPHQL_SUBSCRIPTION_URL || 'ws://localhost:3001/graphql'),
      '__MOCK_GQL__': JSON.stringify(mockGql),
      '__DEV__': JSON.stringify(dev),
    }),
    // https://github.com/jmblog/how-to-optimize-momentjs-with-webpack
    // You can remove this if you don't use Moment.js:
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
  ]

  if (dev) {
    plugins = [
      ...plugins,
      new webpack.HotModuleReplacementPlugin(),
      new CaseSensitivePathsPlugin(),
      new WatchMissingNodeModulesPlugin('node_modules'),
    ]
  } else {
    plugins = [
      new CopyWebpackPlugin([
        {
          from: path.join(__dirname, '../../static'),
          to: constants.outputDir,
        },
      ]),
      ...plugins,
      new MiniCssExtractPlugin({
        filename: constants.outputCssFile,
        chunkFilename: constants.outputCssChunkFiles,
      }),
      new InjectManifest({
        importWorkboxFrom: 'disabled',
        swSrc: path.join(__dirname, '../../static/service-worker.js'),
      }),
    ]
  }

  return plugins
}
