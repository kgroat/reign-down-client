import * as React from 'react'
import { loader } from './Loader.pcss'

const Loader = () => <div className={loader}></div>
export default Loader
