
import * as React from 'react'
import { shallow } from 'enzyme'

import Message from './Message'

describe(Message.name, () => {
  it('should render the given class', () => {
    const testClass = 'myClass'
    const mounted = shallow(<Message className={testClass}>something</Message>)
    expect(mounted.find('.message').prop('className')).toContain(testClass)
  })

  it('should render the given styles', () => {
    const styles = { display: 'none' }
    const mounted = shallow(<Message style={styles}>something</Message>)
    expect(mounted.find('.message').prop('style')).toBe(styles)
  })
})
