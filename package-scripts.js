
const npsUtils = require('nps-utils')

const jestConfig = './config/jest/config.js'
const webpackConfig = './config/webpack/config.ts'

const tsNodeRun = `ts-node --project config/tsconfig.node.json`

const CI = process.env.CI

function addForCI (cmd, toAdd) {
  return CI ? `${cmd} ${toAdd}` : cmd
}

function ifServerStarted (ifStarted, ifNotStarted) {
  if (CI) {
    return ifNotStarted
  }

  return `
    if curl -s "http://localhost:\${PORT:-3000}" >/dev/null; then
      ${ifStarted}
    else
      ${ifNotStarted}
    fi
  `
}

module.exports = {
  scripts: {
    default: {
      script: 'nps webpack',
      description: 'Starts webpack dev server.',
    },
    mockGql: {
      script: 'nps "webpack --env.mockGql"',
      description: 'Starts the webpack dev server in a mode which does not talk to gql server.',
    },
    install: {
      githooks: {
        script: 'cp .githooks/* .git/hooks/',
        description: 'Installs the custom git hooks',
      },
    },
    webpack: {
      script: `webpack-dev-server --config ${webpackConfig} --env.dev`,
      description: 'Starts webpack dev server.',
    },
    build: {
      script: 'nps build.clean build.app',
      description: 'Builds a production bundle of the application.',
      clean: {
        script: `rm -rf 'public'`,
        description: 'removes the output directory'
      },
      app: {
        script: `NODE_ENV="production" webpack --config ${webpackConfig} --env.prod`,
        description: 'bundles the app',
      },
    },
    test: {
      script: `jest --config ${jestConfig}`,
      description: 'Starts the jest tests for the application.',
      fix: {
        script: `nps "test -u"`,
        description: 'Runs the tests in snapshot-update mode.',
      }
    },
    lint: {
      script: 'nps lint.ts lint.css',
      description: 'Runs tslint and stylelint.',
      ts: {
        script: 'tslint --project .',
        description: 'Runs tslint for the entire project.',
        fix: {
          script: 'nps "lint.ts --fix"',
          description: 'Runs tslint in fix mode.',
        },
      },
      css: {
        script: 'stylelint "src/**/*.{pcss,css}"',
        description: 'Runs stylelint for the entire project.',
        fix: {
          script: 'nps "lint.css --fix"',
          description: 'Runs tslint in fix mode.',
        },
      },
      fix: {
        script: 'nps lint.ts.fix lint.css.fix',
        description: 'Runs tslint and stylelint in fix mode.',
      },
    },
    cy: {
      script: ifServerStarted(`nps cy.open`, `start-server-and-test start "http-get://localhost:3000" cy.open`),
      description: 'Start the webpack dev server and open the cypress test browser',
      startAndRun: {
        script: ifServerStarted(`nps cy.run`, `start-server-and-test start.mockGql "http-get://localhost:3000" cy.run`),
        description: 'Start the webpack dev server and run cypress tests',
      },
      run: {
        script: addForCI(`cypress run`, '--record'),
      },
      open: {
        script: 'cypress open',
        description: 'Open the cypress test browser',
        prod: {
          script: 'cypress open --env configFile=production',
          description: 'Open the cypress test browser pointed at production',
        }
      },
      prod: {
        script: addForCI('cypress run --env configFile=production', '--record'),
        description: 'Run the cypress tests against production',
      },
    },
    check: {
      script: 'nps build.app lint css.types test cy.startAndRun',
      description: 'Runs the checks to validate the application (test & lint).',
    },
    css: {
      types: {
        script: 'tcm -p \'./src/**/*.{pcss,css}\'',
        description: 'Generates typescript typings for css modules.',
        watch: {
          script: 'nps "css.types -w"',
          description: 'Generates typescript typings for css modules in watch mode.',
        },
      },
    },
    generate: {
      component: {
        script: 'yo react:component',
        description: 'Generate a react component',
      },
      container: {
        script: 'yo react:container',
        description: 'Generate a react container',
      },
      pureComponent: {
        script: 'yo react:pureComponent',
        description: 'Generate a react pure component',
      },
    },
    graphql: {
      script: 'nps graphql.clean graphql.download graphql.types graphql.schema',
      description: 'Downloads the newest schema and generates typedefs for app queries.',
      clean: {
        script: 'rm -f schema.json schema.graphql',
        description: 'Cleans up previously downloaded versions of the schema',
      },
      download: {
        script: 'apollo schema:download',
        description: 'Downloads the introspection result for the graphql endpoint.'
      },
      schema: {
        script: 'node ./scripts/printSchema.js',
        description: 'Generates `schema.graphql` from `schema.json`.',
      },
      types: {
        script: 'apollo codegen:generate --target typescript --globalTypesFile="src/globalTypes.ts"',
        description: 'Generates types from graphql queries.',
      },
    },
    storybook: {
      default: {
        script: 'start-storybook --ci -p 9000 -c config/.storybook',
        description: 'Starts storybook at localhost:9000',
      },
      build: {
        script: 'build-storybook -c config/.storybook -o storybook-build',
        description: 'Compiles storybook into static files',
      }
    },
  },
}
