
import * as React from 'react'
import { observer } from 'mobx-react-lite'

import Icon from 'components/Icon'
import Link from 'components/Link'
import { routes } from 'routes'
import { useStores } from 'state'

import {
  header, subtitle, logo, icon, title, nav,
} from './Header.pcss'

export function Header () {
  const { auth } = useStores()

  function renderNav () {
    if (auth.loggedIn) {
      return (
        <nav className={nav}>
          <ul>
            <li><Link href={routes.logout.path}>Log out</Link></li>
          </ul>
        </nav>
      )
    } else {
      return (
        <nav className={nav}>
          <ul>
            <li><Link href={routes.login.path}>Log in</Link></li>
            <li><Link href={routes.signup.path}>Sign up</Link></li>
          </ul>
        </nav>
      )
    }
  }

  return (
    <header className={header}>
      <div className={title}>
        <Link href='/' className={logo}>
          <Icon className={icon} name='reign-down' />
          Reign Down
        </Link>
        <div className={subtitle}>
          A game of conquest.
        </div>
      </div>
      {renderNav()}
    </header>
  )
}

export default observer(Header)
