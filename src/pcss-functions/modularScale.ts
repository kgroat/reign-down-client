
import { modularScale } from './constants'

const { base, unit, ratio } = modularScale

export function ms (factor: number | string, actualBase: number | string = base) {
  if (typeof factor === 'string') factor = parseFloat(factor)
  if (typeof actualBase === 'string') actualBase = parseFloat(actualBase)

  const multiplier = Math.pow(ratio, factor)
  return `${actualBase * multiplier}${unit}`
}
