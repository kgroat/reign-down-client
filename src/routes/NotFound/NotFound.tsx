
import * as React from 'react'

class NotFound extends React.PureComponent {
  render () {
    return (
      <React.Fragment>
        <h1>404</h1>
        <div>
          The page you're looking for can't be found.
        </div>
      </React.Fragment>
    )
  }
}

export default NotFound
