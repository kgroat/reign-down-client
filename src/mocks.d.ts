
import { QueryResult, Query } from 'react-apollo'
import { ApolloError } from 'apollo-client'
import { GraphqlErrorCode } from 'helpers/constants'

type BaseMock<ResultType, Variables> = jest.Mock<JSX.Element, [{
  variables: Variables,
  children: (result: QueryResult<ResultType, Variables>) => JSX.Element,
}]>

export interface MockApolloComponent<ResultType, Variables> extends Query<ResultType, Variables>, BaseMock<ResultType, Variables> {
  displayName: string
  mockSuccessOnce (result: ResultType): void
  mockErrorOnce (error?: DeepPartial<ApolloError>): void
  mockGraphqlErrorCodeOnce (code: GraphqlErrorCode): void
  mockLoadingOnce (): void
}
