import * as Generator from 'yeoman-generator'
import * as fs from 'fs'
import * as path from 'path'

import { createPrompts } from '../../helpers/createPrompts'
import { runTemplate } from '../../helpers/runTemplate'

interface Answers {
  name: string
}

const COMPONENTS_ROOT = 'src/components'
const TEMPLATES_DIR = path.join(__dirname, 'templates')

class ComponentGenerator extends Generator {
  private static readonly prompts = createPrompts<Answers>({
    name: {
      type: 'input',
      message: 'What would you like to name your component (should be camelCase)?',
    },
  })

  private _templateDirFiles: string[]
  private _answers: Answers

  readTemplatesDir () {
    this._templateDirFiles = fs
      .readdirSync(TEMPLATES_DIR)
      .map(rel => path.join(TEMPLATES_DIR, rel))
      .filter(item => {
        const stats = fs.statSync(item)
        return stats.isFile()
      })
  }

  async promptUser () {
    this._answers = await this.prompt(ComponentGenerator.prompts) as Answers
  }

  generateModule () {
    this.log('generating files...')
    const componentDir = this.destinationPath(path.join(COMPONENTS_ROOT, this._answers.name))
    if (!fs.existsSync(componentDir)) {
      console.log('creating directory...')
      fs.mkdirSync(componentDir, { recursive: true })
    }

    const templateContext = {
      lowerName: this._answers.name[0].toLowerCase() + this._answers.name.substring(1),
      UpperName: this._answers.name[0].toUpperCase() + this._answers.name.substring(1),
    }

    this._generateFiles(componentDir, templateContext)
  }

  private _generateFiles (outputDir: string, templateContext: any) {
    this._templateDirFiles.forEach(templateFilePath => {
      const templateStr = fs.readFileSync(templateFilePath).toString()

      const fileName = runTemplate(path.basename(templateFilePath), templateContext)
      const fileContent = runTemplate(templateStr, templateContext)

      fs.writeFileSync(
        path.join(outputDir, fileName),
        fileContent,
      )
    })
  }
}

export = ComponentGenerator
