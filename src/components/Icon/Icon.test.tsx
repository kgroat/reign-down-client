
import * as React from 'react'
import { shallow } from 'enzyme'

import Icon from './Icon'

describe(Icon.name, () => {
  it('should render with the `user` icon if that is the `name` provided', () => {
    const mounted = shallow(<Icon name='user' />)
    expect(mounted.find('use')).toMatchSnapshot()
  })
  it('should render with the `className` if specified', () => {
    const testClass = 'myTestClass'

    const mounted = shallow(<Icon className={testClass} name='star' />)
    expect(mounted.find('i').prop('className')).toContain(testClass)
  })
  it('should render with the `style` if specified', () => {
    const testStyle = { color: 'purple' }

    const mounted = shallow(<Icon style={testStyle} name='star' />)
    expect(mounted.find('i').prop('style')).toBe(testStyle)
  })
})
