
import { observable, action, reaction } from 'mobx'
import cssVars from 'css-vars-ponyfill'

import {
  getDefault,
  getContrastingColor,
  makeCompliant,
  printRgb,
  defaultColors
} from '../../pcss-functions'

import autoSave from '../autoSave'

export interface ColorProp {
  color?: ColorStore
}

export type ColorName = keyof typeof defaultColors
export const allColors = Object.keys(defaultColors) as ColorName[]

class ColorStore {
  constructor () {
    reaction(
      () => this.primary,
      () => this._setColor('primary', this.primary),
    )
    reaction(
      () => this.success,
      () => this._setColor('success', this.success),
    )
    reaction(
      () => this.warning,
      () => this._setColor('warning', this.warning),
    )
    reaction(
      () => this.drab,
      () => this._setColor('drab', this.drab),
    )
    reaction(
      () => this.blood,
      () => this._setColor('blood', this.blood),
    )
  }

  @observable black: string = getDefault('black')
  @observable white: string = getDefault('white')

  @observable background: string = getDefault('black')
  @observable foreground: string = getDefault('white')
  @observable primary: string = getDefault('primary')
  @observable success: string = getDefault('success')
  @observable warning: string = getDefault('warning')
  @observable drab: string = getDefault('drab')
  @observable blood: string = getDefault('blood')

  @action
  resetColors () {
    this.black = getDefault('black')
    this.white = getDefault('white')
    this.primary = getDefault('primary')
    this.success = getDefault('success')
    this.warning = getDefault('warning')
    this.drab = getDefault('drab')
    this.blood = getDefault('blood')
  }

  private _setColor (name: ColorName, color: string) {
    const lightColor = makeCompliant(color, this.black)
    const darkColor = makeCompliant(color, this.white)

    cssVars({
      variables: {
        [`--${name}`]: color,
        [`--${name}-rgb`]: printRgb(color),
        [`--${name}-contrast`]: getContrastingColor(color),
        [`--${name}-light`]: lightColor.toString(),
        [`--${name}-light-rgb`]: printRgb(lightColor),
        [`--${name}-dark`]: darkColor.toString(),
        [`--${name}-dark-rgb`]: printRgb(darkColor),
      },
    })
  }
}

export const color = autoSave('color')(new ColorStore())
