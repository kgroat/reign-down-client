
import * as React from 'react'
import { shallow } from 'enzyme'

import $UpperName from './$UpperName'

describe($UpperName.name, () => {
  it('should mount without throwing an error', () => {
    expect(shallow(<$UpperName />)).toMatchSnapshot()
  })
})
