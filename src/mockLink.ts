
import { SchemaLink } from 'apollo-link-schema'
import { makeExecutableSchema, addMockFunctionsToSchema, IMocks } from 'graphql-tools'

const typeDefs = require('!!text-loader!../schema.graphql')
const schema = makeExecutableSchema({
  typeDefs,
  resolverValidationOptions: {
    requireResolversForResolveType: false,
  },
})

const mocks: IMocks = {
  ID: () => Math.floor(Math.random() * 0xFFFFFFFF).toString(16),
  String: () => 'Test String',
  Int: () => 42,
}

addMockFunctionsToSchema({ schema, mocks })

export const mockLink = new SchemaLink({ schema })
