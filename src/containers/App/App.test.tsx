
import * as React from 'react'
import { shallow } from 'enzyme'

import App from './App'

jest.mock('react-apollo')
jest.mock('state/MobxProvider')
jest.mock('../../graphqlClient')

describe(App.name, () => {
  it('should mount without throwing an error', () => {
    expect(shallow(<App />)).toMatchSnapshot()
  })
})
