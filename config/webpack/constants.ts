
import * as path from 'path'

function resolve (givenPath: string) {
  return path.join(__dirname, '../..', givenPath)
}

export function buildConstants (dev: boolean) {
  return {
    entrypoint: dev
      ? resolve('src/index.dev.tsx')
      : resolve('src/index.tsx'),
    srcDir: resolve('src'),
    staticDir: resolve('static'),
    outputDir: resolve('public'),
    tsconfig: resolve('tsconfig.json'),
    htmlPath: resolve('static/index.html'),
    outputCssFile: 'static/css/[name].[contenthash:8].css',
    outputCssChunkFiles: 'static/css/[id].css',
  }
}

export type Constants = ReturnType<typeof buildConstants>
