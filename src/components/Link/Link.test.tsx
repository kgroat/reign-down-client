
import * as React from 'react'
import { shallow } from 'enzyme'

import Link from './Link'

describe(Link.name, () => {
  it('should render an anchor tag', () => {
    const mounted = shallow(<Link href='/test' />)
    expect(mounted.find('a')).toHaveLength(1)
  })

  it('should render the given href', () => {
    const testHref = '/my-test/url'
    const mounted = shallow(<Link href={testHref} />)
    expect(mounted.find('a').prop('href')).toBe(testHref)
  })

  it('should render the given class', () => {
    const testClass = 'myClass'
    const mounted = shallow(<Link className={testClass} href='/test' />)
    expect(mounted.find('a').prop('className')).toContain(testClass)
  })

  it('should render the given styles', () => {
    const styles = { display: 'none' }
    const mounted = shallow(<Link style={styles} href='/test' />)
    expect(mounted.find('a').prop('style')).toBe(styles)
  })
})
