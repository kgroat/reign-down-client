
import * as React from 'react'

import Landing from './Landing'
import Login from './Login'
import Logout from './Logout'
import NotFound from './NotFound'
import Signup from './Signup'

interface RouteDefinition {
  /**
   * The path this route should be mounted on
   */
  path: string

  /**
   * The component that should be rendered by this route
   */
  component: React.ComponentType

  /**
   * If true, don't allow sub-routes
   */
  exact?: boolean

  /**
   * If true, don't render header / bottom buttons
   */
  chromeless?: boolean

  /**
   * Whether the route should be available when logged in
   * - If `show`, the route will only be available if logged in
   * - If `hide`, the route will only be available if *not* logged in
   * - If not supplied (undefined), will *always* be available
   */
  authenticated?: 'show' | 'hide'
}

// This is to ensure the type-safety of `routes` while still allowing fields not in RouteDefinition
function verifyHack<T extends { [key: string]: RouteDefinition }> (obj: T): T {
  return obj
}

export const routes = verifyHack({
  landing: {
    path: '/',
    component: Landing,
    exact: true,
  },
  signup: {
    path: '/signup',
    component: Signup,
    exact: true,
    chromeless: true,
    authenticated: 'hide',
  },
  login: {
    path: '/login',
    component: Login,
    exact: true,
    chromeless: true,
  },
  logout: {
    path: '/logout',
    component: Logout,
    exact: true,
  },
  // NOTE: This should always be the last route in the list.
  notFound: {
    path: '/',
    component: NotFound,
  },
})

export const chromelessRouteArray = Object.keys(routes)
  .filter(key => routes[key] && routes[key].chromeless)
  .map(key => {
    const route = routes[key] as RouteDefinition
    return {
      ...route,
      key,
    }
  })

export const mainRouteArray = Object.keys(routes)
  .filter(key => routes[key] && !routes[key].chromeless)
  .map(key => {
    const route = routes[key] as RouteDefinition
    return {
      ...route,
      key,
    }
  })
