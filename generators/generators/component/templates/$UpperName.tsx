
import * as React from 'react'
import * as classNames from 'classnames'

import { $lowerName } from './$UpperName.pcss'

interface Props {
  className?: string
  style?: React.CSSProperties
}

export default class $UpperName extends React.Component<Props> {
  render () {
    const { className, style } = this.props

    return (
      <div
        className={classNames($lowerName, className)}
        style={style}
      >
        $UpperNameComponent Content
      </div>
    )
  }
}
