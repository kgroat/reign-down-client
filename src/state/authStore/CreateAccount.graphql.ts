
import gql from 'graphql-tag'

export default gql`
mutation CreateAccount(
  $username: String!,
  $password: String!,
  $verifyPassword: String!,
) {
  createUser (
    username: $username,
    password: $password,
    verifyPassword: $verifyPassword,
  ) {
    _id
  }
}
`
