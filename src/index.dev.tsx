
import './global-css/index.pcss'

import * as React from 'react'
import * as ReactDOM from 'react-dom'
import 'roboto-fontface/css/roboto/roboto-fontface.css'

import App from 'containers/App/index.dev'

ReactDOM.render(
  <App />,
  document.getElementById('root'),
)
