
import * as React from 'react'
import { Redirect } from 'react-router'
import { observer, inject } from 'mobx-react'

import { FetchResultError } from 'errors'
import { routes } from 'routes'
import { AuthProp } from 'state/authStore'

import Button from 'components/Button'
import Link from 'components/Link'
import Message from 'components/Message'

import { login, messages, loginForm, formFields, signupLink } from './Login.pcss'

interface Props extends AuthProp {
}

interface State {
  username: string
  password: string
  errors: string[]
}

class LoginBase extends React.Component<Props, State> {
  state: State = {
    username: '',
    password: '',
    errors: [],
  }

  private _onSubmit = async (evt?: React.FormEvent) => {
    if (evt) {
      evt.preventDefault()
    }
    const { auth } = this.props
    const { username, password } = this.state

    if (!username || !password) {
      this.setState({ errors: ['Please supply username and password'] })
      return
    }

    try {
      await auth!.login({ username, password })
    } catch (err) {
      if (err instanceof FetchResultError) {
        this.setState({ errors: err.errors })
      } else {
        this.setState({ errors: [err.message] })
      }
    }
  }

  private _changeUsername = (evt: React.ChangeEvent<HTMLInputElement>) =>
    this.setState({ username: evt.currentTarget.value })

  private _changePassword = (evt: React.ChangeEvent<HTMLInputElement>) =>
    this.setState({ password: evt.currentTarget.value })

  private _renderErrors = () => {
    return this.state.errors.map(err => (
      <Message color='blood'>{err}</Message>
    ))
  }

  render () {
    const { auth } = this.props
    const { username, password } = this.state

    if (auth && auth.loggedIn) {
      return <Redirect to='/' />
    }

    return (
      <div className={login}>
        <div className={messages}>
          {this._renderErrors()}
        </div>
        <form className={loginForm} onSubmit={this._onSubmit}>
          <h1>Log in</h1>
          <div className={formFields}>
            <label htmlFor='username'>
              Username
              <input
                id='username'
                name='username'
                autoComplete='username'
                onChange={this._changeUsername}
                value={username} />
            </label>
            <label htmlFor='password'>
              Password
              <input
                id='password'
                name='password'
                type='password'
                autoComplete='current-password'
                onChange={this._changePassword}
                value={password} />
            </label>
            <Button type='submit' color='primary' onClick={this._onSubmit} size='x-large'>
              Log in
            </Button>
            <Link href={routes.signup.path} className={signupLink}>
              Create an account
            </Link>
          </div>
        </form>
      </div>
    )
  }
}

export default inject('auth')(observer(LoginBase))
