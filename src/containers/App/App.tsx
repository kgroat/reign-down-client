import * as React from 'react'
import { ApolloProvider } from 'react-apollo'
import MobxProvider from 'state/MobxProvider'
import { graphqlClient } from '../../graphqlClient'

import Router from 'containers/Router'

class App extends React.Component {
  public render () {
    return (
      <MobxProvider>
        <ApolloProvider client={graphqlClient}>
          <Router />
        </ApolloProvider>
      </MobxProvider>
    )
  }
}

export default App
