
import * as React from 'react'
import { ApolloError } from 'apollo-client'
import { QueryResult } from 'react-apollo'

import { MockApolloComponent } from '../mocks'

export const ApolloProvider = 'ApolloProvider'

export const Query: MockApolloComponent<any, any> = jest.fn(() => { return <div></div> }) as any
Query.displayName = 'Query'

function createMockedQueryResult (overrides: Partial<QueryResult<any, any>>): QueryResult<any, any> {
  return {
    data: null,
    loading: false,
    client: null as any,
    networkStatus: 0 as any,
    variables: null,
    startPolling: () => null,
    stopPolling: () => null,
    subscribeToMore: () => () => null,
    updateQuery: () => null,
    refetch: () => Promise.resolve({} as any),
    fetchMore: () => Promise.resolve({} as any),
    ...overrides,
  }
}

Query.mockLoadingOnce = () => {
  Query.mockImplementationOnce(({ variables, children }) => {
    return children(createMockedQueryResult({
      loading: true,
      variables,
      networkStatus: 1 as any,
    }))
  })
}

Query.mockErrorOnce = (error: ApolloError = {} as any) => {
  Query.mockImplementationOnce(({ variables, children }) => {
    return children(createMockedQueryResult({
      error,
      variables,
      networkStatus: 8 as any,
    }))
  })
}

Query.mockSuccessOnce = (data) => {
  Query.mockImplementationOnce(({ variables, children }) => {
    return children(createMockedQueryResult({
      data,
      variables,
      networkStatus: 7 as any,
    }))
  })
}

Query.mockGraphqlErrorCodeOnce = (code: any) => {
  Query.mockErrorOnce({
    graphQLErrors: [{
      extensions: {
        code,
      },
    }],
  })
}
