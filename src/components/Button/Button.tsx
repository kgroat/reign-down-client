import * as React from 'react'

import * as styles from './Button.pcss'
import { ColorName } from 'state/colorStore'

interface Props {
  id?: string
  children: React.ReactNode
  type?: string
  className?: string
  color?: typeof DEFAULT | ColorName
  size?: typeof DEFAULT | 'small' | 'large' | 'x-large'
  onClick?: () => void
}

const DEFAULT = 'default'
const DEFAULT_TYPE = 'button'

const Button = (props: Props) => {
  const { id, children, type = DEFAULT_TYPE, className, color = DEFAULT, size = DEFAULT, onClick } = props

  const classes = [styles.button]
  if (className) {
    classes.push(className)
  }

  if (color !== DEFAULT) {
    classes.push(styles[`${color}-color`])
  }

  if (size !== DEFAULT) {
    classes.push(styles[`${size}-size`])
  }

  function _handleClick (ev: React.MouseEvent<HTMLButtonElement>) {
    if (onClick) {
      ev.preventDefault()
      onClick()
    }
  }

  return (
    <button type={type} id={id} className={classes.join(' ')} onClick={_handleClick}>
      {children}
    </button>
  )
}

export default Button
