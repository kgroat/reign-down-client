
import * as React from 'react'
import { Redirect } from 'react-router'
import { observer, inject } from 'mobx-react'

import { AuthProp } from 'state/authStore'

import Link from 'components/Link'

interface State {
  redirect: boolean
  redirectTimeout: any
}

@inject('auth')
@observer
class Logout extends React.Component<AuthProp, State> {
  state: State = {
    redirect: false,
    redirectTimeout: null,
  }

  async componentDidMount () {
    await this.props.auth!.logout()
    const redirectTimeout = setTimeout(() => {
      this.setState({ redirect: true })
    }, 5000)

    this.setState({ redirectTimeout })
  }

  componentWillUnmount () {
    clearTimeout(this.state.redirectTimeout)
  }

  render () {
    if (this.state.redirect) {
      return (
        <Redirect to='/' />
      )
    } else {
      return (
        <div>
          You've been logged out.  You'll be redirected shortly, or you can <Link href='/'>click here</Link>.
        </div>
      )
    }
  }
}

export default Logout
