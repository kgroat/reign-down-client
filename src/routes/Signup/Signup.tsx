
import * as React from 'react'
import { observer, inject } from 'mobx-react'

import { FetchResultError } from 'errors'
import { routes } from 'routes'
import { AuthProp } from 'state/authStore'
import { history } from 'helpers/navigation'

import Button from 'components/Button'
import Link from 'components/Link'
import Message from 'components/Message'

import { signup, messages, signupForm, formFields, loginLink } from './Signup.pcss'

interface Props extends AuthProp {
}

interface State {
  username: string
  password: string
  verifyPassword: string
  errors: string[]
}

class SignupBase extends React.Component<Props, State> {
  state: State = {
    username: '',
    password: '',
    verifyPassword: '',
    errors: [],
  }

  private _onSubmit = async (evt?: React.FormEvent) => {
    if (evt) {
      evt.preventDefault()
    }
    const { auth } = this.props
    const { username, password, verifyPassword } = this.state

    if (!username || !password || !verifyPassword) {
      this.setState({ errors: ['Please supply all fields'] })
      return
    }

    try {
      await auth!.createAccount({
        username,
        password,
        verifyPassword,
      })

      history.replace(routes.login.path)
    } catch (err) {
      if (err instanceof FetchResultError) {
        this.setState({ errors: err.errors })
      } else {
        this.setState({ errors: [err.message] })
      }
    }
  }

  private _changeUsername = (evt: React.ChangeEvent<HTMLInputElement>) =>
    this.setState({ username: evt.currentTarget.value })

  private _changePassword = (evt: React.ChangeEvent<HTMLInputElement>) =>
    this.setState({ password: evt.currentTarget.value })

  private _changeVerifyPassword = (evt: React.ChangeEvent<HTMLInputElement>) =>
    this.setState({ verifyPassword: evt.currentTarget.value })

  private _renderErrors = () => {
    return this.state.errors.map(err => (
      <Message color='blood'>{err}</Message>
    ))
  }

  render () {
    const { username, password, verifyPassword } = this.state

    return (
      <div className={signup}>
        <div className={messages}>
          {this._renderErrors()}
        </div>
        <form className={signupForm} onSubmit={this._onSubmit}>
          <h1>Sign up</h1>
          <div className={formFields}>
            <label htmlFor='username'>
              Username
              <input
                id='username'
                name='username'
                autoComplete='username'
                onChange={this._changeUsername}
                value={username} />
            </label>
            <label htmlFor='password'>
              Password
              <input
                id='password'
                name='password'
                type='password'
                autoComplete='new-password'
                onChange={this._changePassword}
                value={password} />
            </label>
            <label htmlFor='verify-password'>
              Verify Password
              <input
                id='verify-password'
                name='verify-password'
                type='password'
                autoComplete='new-password'
                onChange={this._changeVerifyPassword}
                value={verifyPassword} />
            </label>
            <Button type='submit' color='primary' onClick={this._onSubmit} size='x-large'>
              Sign up
            </Button>
            <Link href={routes.login.path} className={loginLink}>
              Log in instead
            </Link>
          </div>
        </form>
      </div>
    )
  }
}

export default inject('auth')(observer(SignupBase))
