
import * as React from 'react'

import { classes } from 'helpers/classNames'

import { icon, svgIcon } from './Icon.pcss'

export type IconName = keyof typeof iconMap

interface Props {
  name: IconName
  className?: string
  style?: React.CSSProperties
}

function makeSvgIcon (iconUrl: string, { id = 'Content', style = {} }: { id?: string, style?: React.CSSProperties } = {}) {
  return (
    <svg
      viewBox='0 0 1000 1000'
      className={svgIcon}
      style={style}
      // if all svgs are gona be nested in a button, cool
      // if not, these a11y attrs may need to change
      aria-hidden='true'
      focusable='false'
      role='presentation'
    >
      <use xlinkHref={`${iconUrl}#${id}`}/>
    </svg>
  )
}

export const iconMap = {
  'star': makeSvgIcon(require('./icons/filled-star.svg')),
  'empty-star': makeSvgIcon(require('./icons/empty-star.svg')),
  'half-star': makeSvgIcon(require('./icons/half-star.svg')),

  'user': makeSvgIcon(require('./icons/user.svg')),
  'menu': makeSvgIcon(require('./icons/menu.svg')),
  'edit': makeSvgIcon(require('./icons/edit.svg')),
  'close': makeSvgIcon(require('./icons/close.svg')),
  'check': makeSvgIcon(require('./icons/check.svg')),
  'search': makeSvgIcon(require('./icons/search.svg')),

  'chevron-up': makeSvgIcon(require('./icons/chevron.svg')),
  'chevron-down': makeSvgIcon(require('./icons/chevron.svg'), { style: { transform: 'rotate(180deg)' } }),
  'chevron-left': makeSvgIcon(require('./icons/chevron.svg'), { style: { transform: 'rotate(-90deg)' } }),
  'chevron-right': makeSvgIcon(require('./icons/chevron.svg'), { style: { transform: 'rotate(90deg)' } }),

  'npm': makeSvgIcon(require('./icons/npm.svg')),
  'github': makeSvgIcon(require('./icons/github.svg')),

  'reign-down': makeSvgIcon(require('./icons/supremacist.svg')),
}

const im = {
  ...iconMap,
}

export const Icon = ({ name, className, style }: Props) => {
  return <i className={classes([icon, className])} style={style}>{im[name]}</i>
}

export default Icon
