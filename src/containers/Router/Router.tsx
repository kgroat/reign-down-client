
import * as React from 'react'
import { observer } from 'mobx-react-lite'
import { Router as DomRouter, Route, Switch, Redirect } from 'react-router-dom'

import { history } from 'helpers/navigation'

import Header from 'containers/Header'
import Styler from 'containers/Styler'
import InstallButton from 'containers/InstallButton'

import { content } from './Router.pcss'

import { chromelessRouteArray, mainRouteArray } from '../../routes'
import { useStores } from 'state'

export function Router () {
  const { auth } = useStores()

  function renderRoutes (routes: typeof chromelessRouteArray) {
    const loggedIn = auth.loggedIn
    return routes
      .filter(({ authenticated }) => (
        authenticated === undefined ||
        authenticated === 'show' && loggedIn ||
        authenticated === 'hide' && !loggedIn
      ))
      .map(({
        key,
        path,
        component: Component,
        exact,
      }) => (
        <Route
          key={key}
          path={path}
          component={(props: any) => <Component {...props} />}
          exact={exact}
        />
      ))
  }

  return (
    <DomRouter history={history}>
      <Switch>
        {renderRoutes(chromelessRouteArray)}
        <Route path='/'>
          <React.Fragment>
            <Header />
            <main className={content}>
              <Switch>
                {renderRoutes(mainRouteArray)}
                <Redirect to='/' />
              </Switch>
            </main>
            <footer>
              <Styler />
              <InstallButton />
            </footer>
          </React.Fragment>
        </Route>
      </Switch>
    </DomRouter>
  )
}

export const R = observer(Router)
export default R
