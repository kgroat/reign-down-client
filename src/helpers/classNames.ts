
export function classes (classNames: (string | null | undefined)[]) {
  return classNames
    .filter(c => c)
    .filter((c, i) => {
      return classNames.slice(i + 1).indexOf(c) < 0
    })
    .join(' ')
}
