
import { ApolloError } from 'apollo-client'
import { GraphqlErrorCode } from './constants'

export function containsErrorCode ({ graphQLErrors }: ApolloError, code: GraphqlErrorCode): boolean {
  if (!graphQLErrors) {
    return false
  }

  return graphQLErrors.some(err => (
    err.extensions ? err.extensions.code === code : false
  ))
}

export function isNotFoundError (error: ApolloError) {
  return containsErrorCode(error, GraphqlErrorCode.NotFound)
}
