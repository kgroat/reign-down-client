
import * as React from 'react'
import * as classNames from 'classnames'

import { pushOrReplace } from 'helpers/navigation'

import { link } from './Link.pcss'

interface Props {
  href: string
  state?: any

  className?: string
  style?: React.CSSProperties
}

export default class Link extends React.PureComponent<Props> {
  private _onClick = (evt: React.MouseEvent) => {
    // pressing meta (command on macs) or control while clicking should still open the link in a new tab
    // pressing alt while clicking should still save link target
    // pressing shift while clicking should still open the link in a new window
    // right-clicking should still open the context menu
    // middle-clicking should still open the link in a new tab
    if (evt.metaKey || evt.ctrlKey || evt.altKey || evt.shiftKey || evt.button !== 0) {
      return
    }

    // if it's a normal left-click without any modifiers it would normally just navigate
    // instead, we want to force navigation using the history object
    evt.preventDefault()
    const { href, state } = this.props
    pushOrReplace(href, state)
  }

  render () {
    const {
      href,
      children,
      className,
      style,
    } = this.props

    return (
      <a
        className={classNames(link, className)}
        href={href}
        onClick={this._onClick}
        style={style}
      >
        {children}
      </a>
    )
  }
}
