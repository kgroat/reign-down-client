
import * as React from 'react'
import { tooltip, message, icon, visible } from './Tooltip.pcss'

import { getRandomId } from '../../helpers/randomId'

interface Props { style?: React.CSSProperties, children: React.ReactNode }
interface State { hidden: boolean }

type keydownFn = (e: KeyboardEvent) => void

class Tooltip extends React.PureComponent<Props, State> {
  state = { hidden: true }
  handleEnter = (): void => { this.setState({ hidden: false }) }
  handleLeave = (): void => { this.setState({ hidden: true }) }

  handleKeydown: keydownFn = e => {
    if (e.key !== 'Escape') { return }
    this.handleLeave()
  }

  listener: (string | keydownFn | boolean)[] = ['keydown', this.handleKeydown, false]
  componentDidMount (): void { document.addEventListener.apply(null, this.listener) }
  componentWillUnmount (): void { document.removeEventListener.apply(null, this.listener) }

  describedById = getRandomId()

  render () {
    const { style } = this.props
    const { hidden } = this.state
    const messageClassName = `${message} ${!hidden ? visible : ''}`

    return (
      <button
        className={tooltip}
        aria-describedby={this.describedById}
        aria-controls={this.describedById}
        onFocus={this.handleEnter}
        onBlur={this.handleLeave}
        onMouseEnter={this.handleEnter}
        onMouseLeave={this.handleLeave}
        style={style}
      >
        <span className={icon}>&#8505;</span>
        <span
          id={this.describedById}
          className={messageClassName}
          aria-hidden={hidden}
          role='tooltip'
        >{this.props.children}</span>
      </button>
    )
  }
}

export default Tooltip
