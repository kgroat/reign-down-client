import * as webpack from 'webpack'

import * as pcssFunctions from '../../src/pcss-functions'
import * as MiniCssExtractPlugin from 'mini-css-extract-plugin'

import { browsers } from './browsers'

export const styleRule = (dev: boolean): webpack.RuleSetRule => ({
  test: /\.p?css$/,
  use: [
    dev ? 'style-loader' : MiniCssExtractPlugin.loader,
    {
      loader: 'css-loader',
      options: {
        modules: true,
        localIdentName: dev ? '[name]_[local]--[hash:base64:4]' : '[hash:base64:8]',
        importLoaders: 1,
      },
    },
    'typed-css-modules-loader?noEmit',
    {
      loader: 'postcss-loader',
      options: {
        ident: 'postcss',
        plugins: () => [
          require('postcss-import'),
          require('postcss-mixins'),
          require('postcss-each'),
          require('postcss-for'),
          require('postcss-simple-vars'),
          require('postcss-calc'),
          require('postcss-flexbugs-fixes'),
          require('postcss-functions')({
            functions: pcssFunctions,
          }),
          require('postcss-color-function'),
          require('postcss-preset-env'),
          require('postcss-nested'),
          require('autoprefixer')({
            browsers,
            flexbox: 'no-2009',
          }),
        ],
      },
    },
  ],
})
