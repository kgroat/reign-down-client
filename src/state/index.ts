
import * as React from 'react'
import { MobXProviderContext } from 'mobx-react'
import { ProviderProps } from './MobxProvider'

export function useStores (): ProviderProps {
  return React.useContext<ProviderProps>(MobXProviderContext)
}
