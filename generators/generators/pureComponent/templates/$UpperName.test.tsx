
import * as React from 'react'
import { shallow } from 'enzyme'

import $UpperName from './$UpperName'

describe($UpperName.name, () => {
  it('should render the given class', () => {
    const testClass = 'myClass'
    const mounted = shallow(<$UpperName className={testClass} />)
    expect(mounted.find('.$lowerName').prop('className')).toContain(testClass)
  })

  it('should render the given styles', () => {
    const styles = { display: 'none' }
    const mounted = shallow(<$UpperName style={styles} />)
    expect(mounted.find('.$lowerName').prop('style')).toBe(styles)
  })
})
