import * as React from 'react'
import { storiesOf } from '@storybook/react'
import Tooltip from './index'

const tooltipMessage = `Why are you pointing your screwdrivers like that? They're scientific instruments, not water pistols.`

storiesOf('Tooltip', module)
    .add('Default', () => <Tooltip>{tooltipMessage}</Tooltip>)
