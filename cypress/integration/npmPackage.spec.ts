
describe('The npm-package page', () => {
  beforeEach(() => {
    cy.visit(`${Cypress.config('baseUrl')!}/npm-package/cool`)
  })

  it('should show ratings panel and be accessible on init; ', () => {
    cy.get('#tab-item-ratings').should('have.text', 'ratings')
    cy.get('#panel-ratings').should('be.visible')
    cy.runAxe()
  })

  it('should activate the comments panel and be accessible', () => {
    cy.get('#tab-item-comments')
      .should('have.text', 'comments')
      .click()

    cy.get('#panel-comments').should('be.visible')
    cy.runAxe()
  })
})
