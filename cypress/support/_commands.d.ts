/// <reference types="Cypress" />

declare namespace Cypress {
  interface ShouldHaveCssVarOptions {
    caseSensitive?: boolean
    trim?: boolean
  }

  interface Chainable<Subject = any> {
    shouldHaveCssVar (varName: string, expectedValue: string, opts?: ShouldHaveCssVarOptions)
    runAxe (): void
    console (level: 'log' | 'warn' | 'error', ...args: any[]): void
  }
}
