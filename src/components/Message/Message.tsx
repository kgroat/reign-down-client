
import * as React from 'react'

import * as styles from './Message.pcss'
import { ColorName } from 'state/colorStore'

interface Props {
  children: string
  className?: string
  color?: typeof DEFAULT | ColorName
  style?: React.CSSProperties
}

const DEFAULT = 'default'

export default class Message extends React.PureComponent<Props> {
  render () {
    const { children, className, style, color = DEFAULT } = this.props

    const classes = [styles.message]
    if (className) {
      classes.push(className)
    }

    if (color !== DEFAULT) {
      classes.push(styles[`${color}-color`])
    }

    return (
      <div className={classes.join(' ')} style={style}>
        {children}
      </div>
    )
  }
}
