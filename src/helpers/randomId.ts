// Ripped off from deque - https://dequeuniversity.com/library/aria/sf-basic-patterns/sf-tooltip
const randomNumber = () => 1 + Math.random()
const FAN = () => (randomNumber() * 0x10000 | 0).toString(16).substring(1)
// 4 alpha numeric digits, stringed
const endCap = () => `${FAN()}${FAN()}`
const middle = `-${FAN()}-${FAN()}-${FAN()}-${FAN()}`

export function getRandomId (): string {
  return `${endCap()}${middle}${endCap()}`
}
