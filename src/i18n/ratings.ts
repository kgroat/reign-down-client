
export const labels = {
  easeOfUse: 'Ease of Use',
  documentation: 'Documentation',
  a11y: 'Accessibility',
  communication: 'Communication Visibility',
  community: 'Community',
}

export const descriptions = {
  easeOfUse: 'Rate the ease of use when using this package',
  documentation: 'Rate the level of documentation for this package',
  a11y: 'Rate the level of accessibilty for this package',
  communication: 'Rate the level of visible communication for this package',
  community: 'Rate the community for this package; E.g. answers on stackoverflow, has a dedicated blog, social media discussions, dedicated forum, etc...',
}
