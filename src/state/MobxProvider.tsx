
import * as React from 'react'
import { Provider } from 'mobx-react'

import { AuthProp, auth } from './authStore'
import { ColorProp, color } from './colorStore'

interface Props {
  children?: React.ReactNode,
}

export type ProviderProps =
  & Required<AuthProp>
  & Required<ColorProp>

const stores: ProviderProps = {
  auth,
  color,
}

const MobxProvider = ({ children }: Props) => (
  <Provider {...stores}>
    {children}
  </Provider>
)

export default MobxProvider
