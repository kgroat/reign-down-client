
import { setConfig } from 'react-hot-loader'
import { hot } from 'react-hot-loader/root'

import OrigApp from './App'

setConfig({
  logLevel: 'warn',
})

export default hot(OrigApp)
