
declare const __DEV__: boolean
declare const __MOCK_GQL__: boolean

declare module 'value-equal' {
  const valueEqual: (one: any, two: any) => boolean
  export default valueEqual
}

declare type DeepPartial<T> = {
  [P in keyof T]?: T[P] extends Array<infer U>
    ? Array<DeepPartial<U>>
    : T[P] extends ReadonlyArray<infer U>
      ? ReadonlyArray<DeepPartial<U>>
      : DeepPartial<T[P]>
}
