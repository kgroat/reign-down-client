
import { ApolloError } from 'apollo-client'
import { GraphqlErrorCode } from 'helpers/constants'
import { containsErrorCode, isNotFoundError } from '../graphqlErrors'

describe('grqphqlErrors', () => {
  describe(containsErrorCode.name, () => {
    it('should return true if any of the graphQLErrors has the given code', () => {
      const code = 'SOME_CODE' as GraphqlErrorCode

      const error: DeepPartial<ApolloError> = {
        graphQLErrors: [
          {},
          {},
          { extensions: { code } as any },
          {},
        ],
      }

      const actual = containsErrorCode(error as ApolloError, code)

      expect(actual).toBeTruthy()
    })

    it('should return false if none of the graphQLErrors have the given code', () => {
      const code = 'SOME_CODE' as GraphqlErrorCode

      const error: DeepPartial<ApolloError> = {
        graphQLErrors: [
          {},
          { extensions: { code: 'ANOTHER_CODE' } as any },
          {},
        ],
      }

      const actual = containsErrorCode(error as ApolloError, code)

      expect(actual).toBeFalsy()
    })
  })

  describe(isNotFoundError.name, () => {
    it(`should return true if any of the graphQLErrors has the ${GraphqlErrorCode.NotFound} code`, () => {
      const error: DeepPartial<ApolloError> = {
        graphQLErrors: [
          {},
          { extensions: { code: GraphqlErrorCode.NotFound } as any },
          {},
        ],
      }

      const actual = isNotFoundError(error as ApolloError)

      expect(actual).toBeTruthy()
    })

    it(`should return false if none of the graphQLErrors have the ${GraphqlErrorCode.NotFound} code`, () => {
      const error: DeepPartial<ApolloError> = {
        graphQLErrors: [
          {},
          { extensions: { code: 'ANOTHER_CODE' } as any },
          {},
        ],
      }

      const actual = isNotFoundError(error as ApolloError)

      expect(actual).toBeFalsy()
    })
  })
})
