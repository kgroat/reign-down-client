import { addParameters, configure, addDecorator } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import { themes } from '@storybook/theming'

import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '../../src/global-css/colors.pcss'
import './global-styles.pcss'

const stories = require.context('../../src', true, /\.stories\.tsx$/)

function loadStories () {
  stories.keys().forEach(fileName => stories(fileName))
}

addParameters({
  options: {
    name: 'Reign Down',
    addonPanelInRight: true,
    theme: themes.dark,
  },
})

addDecorator(withInfo({
  inline: true,
  // Typescript props won't show w/out react-docgen-typescript-loader
  // Can occur in another iteration
  // https://github.com/strothj/react-docgen-typescript-loader
  propTables: null,
  // styles: {
  //   header: {
  //     body: {
  //       background: 'inherit',
  //       color: 'inherit',
  //     },
  //   },
  // },
  styles: (style) => {
    return ({
      ...style,
      infoBody: {
        ...style.infoBody,
        background: 'inherit',
        color: 'inherit',
        border: '1px solid rgba(128, 128, 128, 0.5)',
      },
    })
  },
}))

configure(loadStories, module)
